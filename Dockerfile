FROM node

env NODE_OPTIONS="--openssl-legacy-provider"

RUN mkdir /app 
COPY . /app
WORKDIR /app

RUN yarn install
RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000
